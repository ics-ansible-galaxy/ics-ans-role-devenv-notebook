import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_notebook_running(host):
    with host.sudo():
        cmd = host.run('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.strip().split('\n')[1:]])
    assert names == ['notebook']


def test_notebook_index(host):
    cmd = host.run('curl -L http://localhost:8888')
    assert '<title>Jupyter Notebook</title>' in cmd.stdout
